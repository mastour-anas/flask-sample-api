from flask_restplus import Resource, Namespace

from app import db
from app.v1 import v1_api
from ..models.product import Product as ProductModel

product_ns = Namespace('product', description='Product operations')


@product_ns.route('/')
class ProductList(Resource):
    @product_ns.marshal_with(ProductModel.product_resource_model)
    def get(self):
        """Get Products list"""
        tasks = ProductModel.query.all()
        return tasks

    @product_ns.expect(ProductModel.product_resource_model, validate=True)
    @product_ns.marshal_with(ProductModel.product_resource_model)
    def post(self):
        """Create a new product"""
        title = v1_api.payload['title']
        description = v1_api.payload['description']
        price = v1_api.payload['price']

        prod = ProductModel(title=title, description=description, price=price)
        db.session.add(prod)
        db.session.commit() 
        return prod


@product_ns.route('/<int:id>')
class Product(Resource):
    @product_ns.response(404, 'Product not found or you don\'t have permission to view it')
    @product_ns.marshal_with(ProductModel.product_resource_model)
    def get(self, id):
        """Get one task"""
        prod = ProductModel.query.filter_by(id=id).first_or_404()
        return prod

    @product_ns.response(404, 'Product not found or you don\'t have permission to edit it')
    @product_ns.expect(ProductModel.product_resource_model, validate=True)
    @product_ns.marshal_with(ProductModel.product_resource_model)
    def put(self, id):
        """Get one Product"""


        prod = ProductModel.query.filter_by(id=id).first_or_404()

        if 'description' in v1_api.payload:
            prod.description = v1_api.payload['description']
        
        if 'price' in v1_api.payload:
            prod.price = v1_api.payload['price']

        if 'active' in v1_api.payload:
            prod.active = v1_api.payload['active']

        db.session.add(prod)
        db.session.commit()

        return prod

    @product_ns.response(404, 'Product not found or you don\'t have permission to delete it')
    @product_ns.response(204, 'Product deleted')
    def delete(self, id):
        """Delete one Product"""
        prod = ProductModel.query.filter_by(id=id).first_or_404()

        db.session.delete(prod)
        db.session.commit()

        return '', 204
