from flask_restplus import fields
from sqlalchemy import ForeignKey
from app import db
from app.v1 import v1_api


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(250))
    price = db.Column(db.Integer, default=0)
    active = db.Column(db.Boolean(), default=False)

    product_resource_model = v1_api.model('Product', {
        'id': fields.Integer(readOnly=True, description='The product unique identifier. ReadOnly.'),
        'title': fields.String(required=True, description='The product name'),
        'description': fields.String(description='Bla bla bla'),
        'price': fields.Integer(required=True, description='The price of product')
    })
